package queue

import "github.com/rs/zerolog/log"

type Part struct {
	A string
	B int64
}

func NewPart() *Part {
	log.Info().Msg("new part")
	return &Part{A: "asd", B: 777}
}
