module test_b

go 1.13

require (
	github.com/rs/zerolog v1.20.0
	github.com/spf13/viper v1.7.1
)
